from django.test import LiveServerTestCase
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
import unittest


class NewVisitorTest(LiveServerTestCase):

    def setUp(self):
        self.browser = webdriver.Firefox()
        self.browser.implicitly_wait(3)

    def tearDown(self):
        self.browser.quit()

    def check_for_row(self, row_text, idT, tag):
        table = self.browser.find_element_by_id(idT)
        rows = table.find_elements_by_tag_name(tag)
        self.assertIn(row_text, [row.text for row in rows])

    def test_add_new_question_and_view_question_home_page(self):
        # open browser
        self.browser.get(self.live_server_url)
        self.assertIn('Survey', self.browser.title)
        header_text = self.browser.find_element_by_tag_name('h1').text
        self.assertIn('แบบสอบถาม', header_text)
 
        inputquest = self.browser.find_element_by_id('ask_new')
        inputans1 = self.browser.find_element_by_id('ans1_new')
        inputans2 = self.browser.find_element_by_id('ans2_new')
        inputans3 = self.browser.find_element_by_id('ans3_new')
        inputans4 = self.browser.find_element_by_id('ans4_new')
        # check placeholder of input tab
        self.assertEqual(
                inputquest.get_attribute('placeholder'),
                'What time is it?'
        )
        self.assertEqual(
                inputans1.get_attribute('placeholder'),
                '13.00'
        )
        self.assertEqual(
                inputans2.get_attribute('placeholder'),
                '14.00'
        )
        self.assertEqual(
                inputans3.get_attribute('placeholder'),
                '15.00'
        )
        self.assertEqual(
                inputans4.get_attribute('placeholder'),
                '16.00'
        )
        # input data of question
        inputquest.send_keys('What is this?')
        inputans1.send_keys('pen')
        inputans2.send_keys('pencil')
        inputans3.send_keys('ruler')
        inputans4.send_keys('rubber')
        self.browser.find_element_by_id('button').click()

        self.check_for_row('Question Answer show time delete', 'id_list_table', 'tr')

if __name__ == '__main__':
    unittest.main(warnings='ignore')
