# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Question',
            fields=[
                ('id', models.AutoField(primary_key=True, serialize=False, verbose_name='ID', auto_created=True)),
                ('quest', models.TextField(default='')),
                ('ans1', models.TextField(default='')),
                ('ans2', models.TextField(default='')),
                ('ans3', models.TextField(default='')),
                ('ans4', models.TextField(default='')),
                ('add_date', models.DateTimeField(blank=True, default=datetime.datetime.now)),
            ],
        ),
    ]
