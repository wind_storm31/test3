from django.shortcuts import redirect, render
from lists.models import Question
from django.utils import timezone
from datetime import datetime, timedelta, date
from django.contrib import auth
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User
from django.core import serializers
import time


def home_page(request):
    if(request.method == 'POST' and request.POST.get(
                                    'delete', '') == 'delete'):
        id_data = request.POST['id_delete']
        Question.objects.get(pk=id_data).delete()
        questions = Question.objects.all()
        return render(request, 'home.html', {'questions': questions})
    if (request.method == 'POST' and request.POST.get(
      'button_send', '') == 'send'):
        if request.POST['ask'] != '':
            date = timezone.now()
            zone = 7
            Question.objects.create(
                             quest=request.POST['ask'],
                             ans1=request.POST['ans1'],
                             ans2=request.POST['ans2'],
                             ans3=request.POST['ans3'],
                             ans4=request.POST['ans4'],
                             answer='',
                             add_date=date.replace(hour=(date.hour+zone) % 24),
                                 )
        questions = Question.objects.all()
        return render(request, 'home.html', {'questions': questions})
    timenow = timezone.now().replace(hour=(timezone.now().hour+7) % 24)
    day = 99999
    if request.method == 'POST' and request.POST.get(
                                    'date', '') == 'send_date':
        day = 99999
    if request.method == 'POST' and request.POST.get(
                                    'day', '') == 'send_day':
        day = 1
    if request.method == 'POST' and request.POST.get(
                                    'week', '') == 'send_week':
        day = 7
    if request.method == 'POST' and request.POST.get(
                                    'month', '') == 'send_month':
        day = 30
    if request.method == 'POST' and request.POST.get(
                                    'year', '') == 'send_year':
        day = 365
    tfilter = timenow - timedelta(days=day)
    questions = Question.objects.filter(add_date__range=(tfilter, timenow))
    return render(request, 'home.html', {'questions': questions})


def answer(request, id_ans):
    if (request.method == 'POST' and request.POST.get(
      'send_ans', '') == 'send_ans'):
        id_q = request.POST['id_ans']
        q = Question.objects.get(id=id_q)
        q.answer = request.POST['ans']
        q.save()
        questions = Question.objects.all()
        return render(request, 'home.html', {'questions': questions})
    questions = Question.objects.all()
    return render(request, 'home.html', {'questions': questions})
