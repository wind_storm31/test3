from django.db import models
from datetime import datetime
from django.core import serializers


class Question(models.Model):
    quest = models.TextField(default='')
    ans1 = models.TextField(default='')
    ans2 = models.TextField(default='')
    ans3 = models.TextField(default='')
    ans4 = models.TextField(default='')
    answer = models.TextField(default='')
    add_date = models.DateTimeField(default=datetime.now, blank=True)
