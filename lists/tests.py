from django.core.urlresolvers import resolve
from django.template.loader import render_to_string
from django.test import TestCase
from django.http import HttpRequest
from lists.models import Question
from lists.views import home_page, answer
import pep8


class HomePageTest(TestCase):

    def test_root_url_resolves_to_home_page_view(self):
        found = resolve('/')
        self.assertEqual(found.func, home_page)

    def test_home_page_returns_correct_html(self):
        request = HttpRequest()
        response = home_page(request)
        expected_html = render_to_string('home.html')
        self.assertEqual(response.content.decode(), expected_html)

    def test_question_can_save_a_POST_request(self):
        request = HttpRequest()
        request.method = 'POST'
        request.POST['ask'] = 'What is this?'
        request.POST['ans1'] = 'pen'
        request.POST['ans2'] = 'pencil'
        request.POST['ans3'] = 'ruler'
        request.POST['ans4'] = 'paper'

        request.POST['button_send'] = 'send'

        response = home_page(request)

        question = Question.objects.first()
        self.assertEqual(question.quest, 'What is this?')
        self.assertEqual(question.ans1, 'pen')
        self.assertEqual(question.ans2, 'pencil')
        self.assertEqual(question.ans3, 'ruler')
        self.assertEqual(question.ans4, 'paper')

    def test_home_page_only_saves_movie_when_necessary(self):
        request = HttpRequest()
        home_page(request)

    def test_home_page_displays_all_list_questions(self):
        Question.objects.create(quest='How old are you?')
        Question.objects.create(quest='What your favorite color?')

        request = HttpRequest()
        response = home_page(request)

        self.assertIn('How old are you?', response.content.decode())
        self.assertIn('What your favorite color?', response.content.decode())

    def test_delete_question(self):
        question = Question()
        question.quest = 'What is this?'
        question.ans1 = 'pen'
        question.ans2 = 'pencil'
        question.ans3 = 'ruler'
        question.ans4 = 'paper'
        question.save()
        request = HttpRequest()
        request.method = 'POST'
        request.POST['id_delete'] = '1'
        request.POST['delete'] = 'delete'

        response = home_page(request)

        self.assertEqual(Question.objects.count(), 0)


class TestCodeFormat(TestCase):

    def test_functional_test(self):
        pep8style = pep8.StyleGuide(show_source=True)
        result = pep8style.check_files([
                 '/home/poo/test3/lists/tests.py'])
        self.assertEqual(result.total_errors, 0,
                         "Found code style errors (and warnings).")

    def test_test(self):
        pep8style = pep8.StyleGuide(show_source=True)
        result = pep8style.check_files([
                 '/home/poo/test3/lists/tests.py'])
        self.assertEqual(result.total_errors, 0,
                         "Found code style errors (and warnings).")

    def test_view(self):
        pep8style = pep8.StyleGuide(show_source=True)
        result = pep8style.check_files([
                 '/home/poo/test3/lists/views.py'])
        self.assertEqual(result.total_errors, 0,
                         "Found code style errors (and warnings).")

    def test_model(self):
        pep8style = pep8.StyleGuide(show_source=True)
        result = pep8style.check_files([
                 '/home/poo/test3/lists/models.py'])
        self.assertEqual(result.total_errors, 0,
                         "Found code style errors (and warnings).")
